from django.db import models

# Create your models here.

class Usuario(models.Model):
	Nombres = models.CharField(max_length = 35)
	Apellido_Paterno = models.CharField(max_length = 35)
	Apellido_Materno = models.CharField(max_length = 35)
	User_Name = models.CharField(max_length = 15)
	Contraseña = models.CharField(max_length = 8)
	DNI = models.CharField(max_length = 10, primary_key = True)
	Status = (('A', 'Activo'),('I', 'Inactivo'))
	Estatus = models.CharField(max_length = 1, choices = Status, default = 'A')
	StatusP = (('A', 'Administrador'), ('U','Usuario'))
	Perfil = models.CharField(max_length = 1, choices = StatusP, default = 'U')

	def NombreCompleto(self):
		cadena = "{0} {1} {2}"
		return cadena.format(self.Apellido_Paterno, self.Apellido_Materno, self.Nombres)

	def __str__(self):
		return self.NombreCompleto()

class Producto(models.Model):
	id = models.AutoField(primary_key = True)
	Descripcion = models.CharField(max_length = 50)
	Cantidad = models.CharField(max_length = 4)
	Precio = models.CharField(max_length = 20)
	Fecha_Creación = models.DateField()

	


			